import click


class ClickError(click.ClickException):
    def show(self):
        click.secho('ERROR: ', fg='red', nl=False, err=True)
        click.echo(self.message)
