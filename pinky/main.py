import os
import shutil

import click

import PIL
from PIL import Image

from .utils import ClickError


@click.group()
# @click.option('--host', default='http://structrs.com')
# @click.option('--app', '-a')
# @click.option('--service', '-s')
@click.pass_context
def main(ctx):
    # TODO: hostname validator?
    # ctx.obj['host'] = host
    # ctx.obj['cfg'] = read_config()
    # ctx.obj['app'] = app
    # ctx.obj['svc'] = service
    pass


@main.command()
@click.option('-s', '--source', help='source path')
@click.option('-d', '--destination', help='destination')
@click.option('--gopro/--no-gopro', default=True, help='use GoPro source')
@click.pass_context
async def xfer(ctx, source, destination, gopro):
    if not source:
        for e in os.listdir('/run/media'):
            e = os.path.join('/run/media', e)
            if os.path.isdir(e):
                dir = e
                break
        if dir:
            for e in os.listdir(dir):
                e = os.path.join(dir, e)
                if os.path.isdir(e):
                    dir = e
                    break
        if not dir:
            raise ClickError('unable to locate source path')
        source = dir
    if not os.path.isdir(source):
        raise ClickError('source is not a directory')
    if gopro:
        source = os.path.join(source, 'DCIM', '100GOPRO')
    if not destination:
        destination = 'pinky-images'
    if not os.path.exists(destination):
        os.makedirs(destination)
    if not os.path.isdir(destination):
        raise ClickError('destination is not a directory')
    files = []
    for e in os.listdir(source):
        full = os.path.join(source, e)
        if os.path.isfile(full) and e[-4:].lower() == '.jpg':
            files.append(e)
    for f in files:
        s = os.path.join(source, f)
        d = os.path.join(destination, f)
        shutil.copy(s, d)
        img = Image.open(s)
        scale = 160.0 / float(img.size[1])
        w = int(float(img.size[0]) * scale)
        thumb = img.resize((w, 160), PIL.Image.ANTIALIAS)
        thumb.save(f'{destination}/{f[:-4]}-thumb.jpg')
        scale = 600.0 / float(img.size[1])
        w = int(float(img.size[0]) * scale)
        full = img.resize((w, 600), PIL.Image.ANTIALIAS)
        full.save(f'{destination}/{f[:-4]}-full.jpg')
